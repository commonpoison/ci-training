<?php
class User_model extends CI_Model
{
	public function insert_user($data)
	{
		$this->db->insert('users', $data);
	}

	public function check_user($email, $username)
	{
		$this->db->select('id');
		$this->db->from('users');
		$this->db->where('email', $email);
		$this->db->or_where('username', $username);
		$query = $this->db->get();

		if($query->num_rows() > 0){
			return false;
		} else {
			return true;
		}
	}

	public function login_user($login_data)
	{
		$this->db->select('id, username, email, first_name, last_name');
		$this->db->from('users');
		$this->db->where($login_data);
		$query = $this->db->get();

		if($query->num_rows() == 1){
			return $query->row_array();
		} else {
			return false;
		}
	}
}
