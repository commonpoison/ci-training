<?php
class Link_stat_model extends CI_Model
{	
	public function get_link_stats($id, $draw = 1, $start = 0, $length = 10)
	{
		$this->db->select('time, ip, browser');
		$this->db->from('link_stats');
		$this->db->where('link_id', $id);
		$this->db->limit($length, $start);
		$query = $this->db->get();
		
		$data = array();
		$data_raw = $query->result_array();
		foreach($data_raw as $dt){
			array_push($data, array(
				mdate("%d %M %Y %h:%i:%s", $dt['time']), 
				$dt['ip'], 
				$dt['browser'])
			);
		}

		$total = $this->db->where('link_id', $id)->from('link_stats')->count_all_results();

		$result = array();
		$result['recordsTotal'] = $total;
		$result['recordsFiltered'] = $total;
		$result['draw'] = $draw; 
		$result['data'] = $data;

		return $result;
	}

	public function insert_link_stat($data)
	{
		$data['time'] = time();

		$this->db->insert('link_stats', $data);
	}
}
