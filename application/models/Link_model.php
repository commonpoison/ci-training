<?php
class Link_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_links()
	{
		$query = $this->db->get('links');
		return $query->result();
	}

	public function insert_link($data)
	{
		$data['expired'] = time() + (24 * 60 * 60);
		$data['created'] = time();

		$this->db->insert('links', $data);
	}

	public function get_active_link_data($code)
	{
		$this->db->select('id, link');
		$this->db->from('links');
		$this->db->where('code', $code);
		$this->db->where('expired >=', time());
		$query = $this->db->get();

		if($query->num_rows() == 1){
			return $query->row_array();
		} else {
			return false;
		}
	}
}
