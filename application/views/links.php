<nav class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">Short URL Generator</a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li><a href="<?php echo base_url('dashboard/view');?>">Home</a></li>
				<li class="active"><a href="#">Links</a></li>
			</ul>
        </div>
	</div>
</nav>
<div class="container">
	<div class="panel-heading">
		<h3 class="panel-title">Links</h3>
	</div>
	<div class="panel-body">
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Code</th>
					<th>URL</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($links as $link){?>
					<tr>
						<td><?= $link->id?></td>
						<td><a href="<?php echo base_url("v/$link->code");?>"><?= $link->code?></a></td>
						<td><?= $link->link?></td>
						<td><a href="<?php echo base_url("link/view_stat/$link->id");?>">View</a></td>
					</tr>
				<?php }?>
			</tbody>
		</table>
	</div>
</div>