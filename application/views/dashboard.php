<nav class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">Short URL Generator</a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">Home</a></li>
				<li><a href="<?php echo base_url('link/view_all');?>">Links</a></li>
			</ul>
        </div>
	</div>
</nav>
<div class="container">
	<div class="panel-heading">
		<h3 class="panel-title">Hi, <?= $this->session->username?>!</h3>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-8">
				<input class="form-control" placeholder="Enter link" name="link" id="link" type="text" autofocus/>
			</div>
			<div class="col-xs-4">
				<input class="btn btn-block" type="submit" id="generate_link_btn" value="Generate"/>
			</div>
		</div>
		<div id="output">
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#generate_link_btn').click(function(){
		$.ajax({
			url: "<?php echo base_url('dashboard/generate_link');?>", 
			type: "POST",
			data: {
				link: $('#link').val(), 
				user_id: <?php echo $this->session->user_id?>
			}, 
			success: function(result){
				$('#output').append(result + "<br/>");
			}
		});
	});
</script>