<div class="container">
	<div class="panel-heading">
		<h3 class="panel-title">Register</h3>
	</div>
	<div class="panel-body">
		<form role="form" method="post" action="<?php echo base_url('user/do_register');?>">
			<fieldset>
				<div class="form-group">
					<input class="form-control" placeholder="Username" name="username" type="text" autofocus/>
				</div>
				<div class="form-group">
					<input class="form-control" placeholder="Password" name="password" type="password" value=""/>
				</div>
				<div class="form-group">
					<input class="form-control" placeholder="Email" name="email" type="email"/>
				</div>
				<div class="form-group">
					<input class="form-control" placeholder="First Name" name="first_name" type="text"/>
				</div>
				<div class="form-group">
					<input class="form-control" placeholder="Last Name" name="last_name" type="text"/>
				</div>
				<input class="btn btn-lg btn-block" type="submit" value="Register"/>
			</fieldset>
		</form>
		<center><b>Have an account? </b><a href="<?php echo base_url('user/login_view'); ?>">Login here</a></center>
	</div>
</div>
