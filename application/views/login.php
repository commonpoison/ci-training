<div class="container">
	<div class="panel-heading">
		<h3 class="panel-title">Login</h3>
	</div>
	<div class="panel-body">
		<form role="form" method="post" action="<?php echo base_url('user/do_login');?>">
			<fieldset>
				<div class="form-group">
					<input class="form-control" placeholder="Username" name="username" type="text" autofocus/>
				</div>
				<div class="form-group">
					<input class="form-control" placeholder="Password" name="password" type="password" value=""/>
				</div>
				<input class="btn btn-lg btn-block" type="submit" value="Login"/>
			</fieldset>
		</form>
		<center><b>Don't have an account? </b><a href="<?php echo base_url('user/register_view'); ?>">Register here</a></center>
	</div>
</div>