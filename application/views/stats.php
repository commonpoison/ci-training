<?php 
	$ajax_url = base_url('link/stat/');
	$ajax_url .= $link_id;
?>
<nav class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">Short URL Generator</a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li><a href="<?php echo base_url('dashboard/view');?>">Home</a></li>
				<li class="active"><a href="<?php echo base_url('link/view_all');?>">Links</a></li>
			</ul>
        </div>
	</div>
</nav>
<div class="container">
	<div class="panel-heading">
		<h3 class="panel-title">Statistics</h3>
	</div>
	<div class="panel-body">
		<table class="display" id="stat_table">
			<thead>
				<tr>
					<th>Time</th>
					<th>IP</th>
					<th>Browser</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#stat_table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": "<?= $ajax_url?>"
		});
	});
</script>
