<html>
	<head>
		<meta charset="utf-8">
		<title>CI Training</title>
		<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.css"); ?>" />
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>
		<script src="<?php echo base_url("assets/js/jquery-3.2.1.min.js"); ?>"></script>
		<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>

	</head>
	<body>
		<?php
			$success_msg= $this->session->flashdata('success_msg');
			$error_msg= $this->session->flashdata('error_msg');

			if ($success_msg){
		?>
		<div class="alert alert-success">
			<?php echo $success_msg; ?>
		</div>
		<?php
			} else if ($error_msg){
		?>
		<div class="alert alert-danger">
			<?php echo $error_msg; ?>
		</div>
		<?php
			}
		?>
