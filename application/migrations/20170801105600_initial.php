<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Initial extends CI_Migration
{
	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			), 'username' => array(
				'type' => 'VARCHAR',
				'constraint' => '20'
			), 'password' => array(
				'type' => 'VARCHAR',
				'constraint' => '50'
			), 'email' => array(
				'type' => 'VARCHAR',
				'constraint' => '50'
			), 'first_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '50'
			), 'last_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '150'
			), 'created' => array(
				'type' => 'INT',
				'constraint' => 11
			), 'updated' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE
			), 'deleted' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE
			)
		));
		$this->dbforge->add_key('id', TRUE); 
		$this->dbforge->create_table('users');


		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			), 'user_id' => array(
				'type' => 'INT',
				'constraint' => 11
			), 'code' => array(
				'type' => 'VARCHAR',
				'constraint' => '50'
			), 'link' => array(
				'type' => 'VARCHAR',
				'constraint' => '255'
			), 'expired' => array(
				'type' => 'INT',
				'constraint' => 11
			), 'created' => array(
				'type' => 'INT',
				'constraint' => 11
			), 'deleted' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE
			)
		));
		$this->dbforge->add_key('id', TRUE); 
		$this->dbforge->create_table('links');


		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			), 'link_id' => array(
				'type' => 'INT',
				'constraint' => 11
			), 'ip' => array(
				'type' => 'VARCHAR',
				'constraint' => '50'
			), 'browser' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			), 'time' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('id', TRUE); 
		$this->dbforge->create_table('link_stats');
	}

	public function down()
	{
		$this->dbforge->drop_table('link_stats');
		$this->dbforge->drop_table('links');
		$this->dbforge->drop_table('users');
	}
}
