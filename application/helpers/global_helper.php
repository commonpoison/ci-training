<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('get_user_agent'))
{
    function get_user_agent()
    {
        static $agent = null;

		if(empty($agent)){
			$agent = $_SERVER['HTTP_USER_AGENT'];

			if ( stripos($agent, 'Firefox') !== false ) {
				$agent = 'Mozilla Firefox';
			} elseif ( stripos($agent, 'MSIE') !== false ) {
				$agent = 'IE';
			} elseif ( stripos($agent, 'iPad') !== false ) {
				$agent = 'iPad';
			} elseif ( stripos($agent, 'Android') !== false ) {
				$agent = 'Android';
			} elseif ( stripos($agent, 'Chrome') !== false ) {
				$agent = 'Google Chrome';
			} elseif ( stripos($agent, 'Safari') !== false ) {
				$agent = 'Safari';
			} elseif ( stripos($agent, 'AIR') !== false ) {
				$agent = 'Air';
			} elseif ( stripos($agent, 'Fluid') !== false ) {
				$agent = 'Fluid';
			}

	 	}

		return $agent;
    }   
}