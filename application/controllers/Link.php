<?php 
class Link extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('global');
		$this->load->helper('date');
		$this->load->model('link_model');
		$this->load->model('link_stat_model');
	}

	public function view_all()
	{
		$data['links'] = $this->link_model->get_links();
		$this->load->view('templates/header');
		$this->load->view('links', $data);
		$this->load->view('templates/footer');
	}

	public function view($code)
	{
		if(!empty($code)){
			$link_detail = $this->link_model->get_active_link_data($code);

			if($link_detail == false){
				show_404();
				return;
			}

			$view_data = array(
				'link_id' => $link_detail['id'], 
				'ip' => $this->input->server('REMOTE_ADDR'), 
				'browser' => get_user_agent()
			);

			$this->link_stat_model->insert_link_stat($view_data);
			$loc = 'location: ' . $link_detail['link'];
			header($loc);
		}
	}

	public function view_stat($id = null)
	{
		if(!empty($id)){
			$data['link_id'] = $id;

			$this->load->view('templates/header');
			$this->load->view('stats', $data);
			$this->load->view('templates/footer');
		} else {
			show_404();
		}
	}

	public function stat($id = null)
	{
		if(isset($id)){
			$data = $this->link_stat_model->get_link_stats(
				$id, 
				$this->input->get('draw'),
				$this->input->get('start'), 
				$this->input->get('length')
			);
			echo json_encode($data);
		}

	}
}