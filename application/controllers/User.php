<?php
class User extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('user_model');
	}

	public function login_view()
	{
		$this->load->view('templates/header');
		$this->load->view('login');
		$this->load->view('templates/footer');
	}

	public function do_login()
	{
		$login_data = array(
			'username' => $this->input->post('username'),
			'password' => md5($this->input->post('password'))
		);

		$user_data = $this->user_model->login_user($login_data);
		if(!$user_data){
			$this->session->set_flashdata('error_msg', 'Invalid username or password');
			redirect('user/login_view');
		} else {
			$this->session->set_userdata('user_id', $user_data['id']);
			$this->session->set_userdata('username', $user_data['username']);
			redirect('dashboard/view');
		}
	}

	public function register_view()
	{
		$this->load->view('templates/header');
		$this->load->view('register');
		$this->load->view('templates/footer');
	}

	public function do_register()
	{
		$user = array(
			'username' => $this->input->post('username'), 
			'password' => md5($this->input->post('password')),
			'email' => $this->input->post('email'),
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name')
		); 

		if ($this->user_model->check_user($user['email'], $user['username'])){
			$this->user_model->insert_user($user);
			$this->session->set_flashdata('success_msg', 'You have been registered successfully!');
			redirect('user/login_view');
		} else {
			$this->session->set_flashdata('error_msg', 'This email/username has been registered');
			redirect('user/register_view');
		}
	}
}
