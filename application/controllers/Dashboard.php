<?php 
class Dashboard extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('string', 'form', 'url'));
		$this->load->model('link_model');
	}

	public function view()
	{
		$this->load->view('templates/header');
		$this->load->view('dashboard');
		$this->load->view('templates/footer');
	}

	public function generate_link()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('link', 'Link', 'callback_url_check');
		$this->form_validation->set_rules('user_id', 'User ID', 'required');
		$this->form_validation->set_message('url_check', 'URL is not valid');
			
		if($this->form_validation->run() === FALSE){
			echo validation_errors();
		} else {
			$code = random_string('alpha', 8);
			$data = array(
				'user_id' => $this->input->post('user_id'), 
				'link' => $this->input->post('link'), 
				'code' => $code
			);

			$this->link_model->insert_link($data);
			echo $code;
		}
	}

	public function url_check($url)
	{
		return filter_var($url, FILTER_VALIDATE_URL);
	}

}